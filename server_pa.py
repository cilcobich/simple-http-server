
import SocketServer
import os
import cgi
from SimpleHTTPServer import SimpleHTTPRequestHandler
from time import strftime, mktime, strptime
from datetime import datetime

PORT = 8080
DB = 'db.txt'
HTML_FORM = 'form.html'
HTML_POST = 'post.html'

URL_FORM = '/form'
URL_POST = '/post'

class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        page = ''

        if self.path == URL_FORM:
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()

            page = self._read_page(HTML_FORM)
            page = page.format('', '', '', '')

        elif self.path == URL_POST:
            last_modified = strftime("%a, %d %b %Y %H:%M:%S GMT",
                                 datetime.fromtimestamp(os.path.getmtime(DB)).timetuple()
                                )

            if 'If-Modified-Since' in self.headers:
                if self._modified_since(last_modified):
                    self.send_response(200)
                else:
                    self.send_response(304)
            else:
                self.send_response(200)

            self.send_header('Content-Type', 'text/html')
            self.send_header('Last-Modified', last_modified)
            self.end_headers()

            try:
                page = self._read_page(HTML_POST)
                with open(DB, 'r') as file_db:
                    page = page.format(file_db.readline(), file_db.readline())
            except IOError:
                self.wfile.write("Error Interno")
        else:
            self._redirect(URL_POST)

        self.wfile.write(page)

        return

    def do_POST(self):
        if self.path == URL_FORM:
            form = cgi.FieldStorage(
                                    fp=self.rfile,
                                    headers=self.headers,
                                    environ={'REQUEST_METHOD': 'POST',
                                             'CONTENT_TYPE': self.headers['Content-Type'],
                                            }
                                    )
            valid = True
            title = ''
            title_error = ''
            post = ''
            post_error = ''

            if form.has_key('title') and form['title'].value != '':
                title = form['title'].value
            else:
                valid = False
                title_error = '*'

            if form.has_key('post') and form['post'].value != '':
                post = form['post'].value
            else:
                valid = False
                post_error = '*'

            if valid:
                try:
                    post = self._read_page(HTML_POST)
                    with open(DB, 'wb') as file_db:
                        file_db.write('{0}\n'.format(form['title'].value))
                        file_db.write('{0}\n'.format(form['post'].value))

                        self._redirect(URL_POST)

                except IOError:
                    self.wfile.write("Error Interno")
            else:
                self.send_response(200)
                self.send_header("Content-Type", "text/html")
                self.end_headers()

                page = self._read_page(HTML_FORM)
                page = page.format(title, title_error, post, post_error)

                self.wfile.write(page)

    def _read_page(self, html_file):
        """
        Devuelve el string html del archivo pasado como argumento.
        """
        try:
            with open(html_file, 'r') as file_html:
                string = file_html.read()
                return string
        except IOError:
            self.wfile.write("Error Interno")

    def _modified_since(self, last_modified):
        """
        Compara la fecha de modificacion del archivo DB
        con 'If-Modified-Since' del header.
        Devuelve un booleano.
        """
        if 'If-Modified-Since' in self.headers:
            db_last_modified = mktime(strptime(last_modified,
                                                 "%a, %d %b %Y %H:%M:%S GMT"))
            request_modified = mktime(strptime(self.headers['If-Modified-Since'],
                                                 "%a, %d %b %Y %H:%M:%S GMT"))
            if db_last_modified == request_modified:
                return False

        return True

    def _redirect(self, url):
        """
        Hace un redireccionamiento a la url que se le
        pase como argumento.
        """
        self.send_response(301)
        self.send_header("Location", url)
        self.end_headers()

try:
    server = SocketServer.TCPServer(('', PORT), MyHandler)
    print 'Started httpserver on port ', PORT
    server.allow_reuse_address = True
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
